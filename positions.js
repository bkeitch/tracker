,{"transmit_time" :"18-11-06 03:56:16" ,"p":[37820408,-122484127],"m":"Position update for S/Y Flutterby. No status message available."}
,{"transmit_time" :"18-11-08 01:21:11" ,"p":[37815808,-122492250],"m":"Going under golden gate bridge, excited to be off, right on!"}
,{"transmit_time" :"18-11-08 01:24:25" ,"p":[35132900, -127871100],"m":"", "iridium_cep":"3.0"}
,{"transmit_time" :"18-11-09 20:27:40" ,"p":[33645302,-129950357],"m":"Steady progress, wind abeam. \"Are we there yet?\" - LS."}
,{"transmit_time" :"18-11-10 19:37:52" ,"p":[32518268,-132044208],"m":"Position update for S/Y Flutterby. No status message available."}
,{"transmit_time" :"18-11-11 18:50:11" ,"p":[29564000,-137423600],"m":"The sun it out. Nearly half way. Music and no-shoes."}
,{"transmit_time" :"18-11-14 00:17:32" ,"p":[27889600,-140267300],"m":"Celebrated 1/2 way Hawaii & Jason's 50th with delicious meal!", "iridium_cep":"3.0"}
,{"transmit_time" :"18-11-15 04:36:09" ,"p":[26151000,-141496500],"m":"Temps warming, sunny, wind abaft. Steady progress, all well.", "iridium_cep":"3.0"}
,{"transmit_time" :"18-11-15 22:23:12" ,"p":[25818465,-143227472],"m":"Chocolate cake, jam and crystal clear blue warm water to swim.", "iridium_cep":"3.0"}
,{"transmit_time" :"18-11-16 22:08:02" ,"p":[23677457,-147449505],"m":"Have seen flying fish, rain and lentils. Are we there yet?"}
,{"transmit_time" :"18-11-18 23:39:41" ,"p":[22164332,-150311900],"m":"Squalls and rain, spinnakers and rainbows. Only 1 day to Hawaii"}
,{"transmit_time" :"18-11-20 04:05:52" ,"p":[20444895,-154454915],"m":"Should see Hawaii tonight, we're nearly there! xoxoxo"}
,{"transmit_time" :"18-11-21 20:15:46" ,"p":[19727740,-155074603],"m":"Anchored in Hilo bay, Hawaii, safe & sound. We made it!"}
,{"transmit_time" :"18-11-22 07:40:12" ,"p":[19342452,-154856255],"m":"Position update for S/Y Flutterby. No status message available."}
,{"transmit_time" :"18-11-28 05:36:30" ,"p":[15056295,-157959918],"m":"Left Hawaii. Dolphins on bow. Muggy but good winds. Missing Ben."}
,{"transmit_time" :"18-11-30 00:58:08" ,"p":[12836920,-159138437],"m":"Position update for S/Y Flutterby. No status message available."}
,{"transmit_time" :"18-12-01 02:27:13" ,"p":[9489800,-160527600],"m":"Position update for S/Y Flutterby. No status message available.", "iridium_cep":"3.0"}
