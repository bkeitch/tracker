<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <title>Track of Yacht Flutterby</title>
  <style type="text/css">
	.olImageLoadError {
			display: none !important; 
	}
  </style>
  <!-- bring in the OpenLayers javascript library version 2.0 (currently in use by Sea Map)
                        (here we bring it from the remote site, but local copies exist for testing) -->

  <script src="http://www.openlayers.org/api/OpenLayers.js"></script>
  <!-- bring in the OpenStreetMap OpenLayers layers.
                        Using this hosted file will make sure we are kept up
                        to date with any necessary changes -->

  <script src="http://www.openstreetmap.org/openlayers/OpenStreetMap.js"></script>
		
  <script type="text/javascript">

	var map;
	var layer_mapnik;
	var layer_tah;
	var layer_seamark;
	var marker;
	var layer_markers;

	// Position and zoomlevel of the map
	// centres on last position
	var lon = 0;
	var lat = 0;
	var zoom = 4;
	var PI = 3.14159265358979323846;
	var totalDistance = 0;
	var language = 'en';

	function jumpTo(lon, lat, zoom) {
		var x = Lon2Merc(lon);
		var y = Lat2Merc(lat);
		map.setCenter(new OpenLayers.LonLat(x, y), zoom);
		return false;
	}

	function Lon2Merc(lon) {
		return 20037508.34 * lon / 180;
	}

	function Lat2Merc(lat) {
		lat = Math.log(Math.tan( (90 + lat) * PI / 360)) / (PI / 180);
		return 20037508.34 * lat / 180;
	}
	/*
	* Uses Haversine formula, assumes world is sphere. Good enough approximation!
	*/
	function distanceRun(lon1, lat1, lon2, lat2) {
		if ((lat1 == lat2) && (lon1 == lon2)) {
				return 0;
		}
		else {
			var radlat1 = Math.PI * lat1/180;
			var radlat2 = Math.PI * lat2/180;
			var theta = lon1-lon2;
			var radtheta = Math.PI * theta/180;
			var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
			if (dist > 1) {
				dist = 1;
			}
			dist = Math.acos(dist);
			dist = dist * 180/Math.PI * 60;
				
		}
		return Math.floor(dist);
	}
	/**
	 * adds the position of the yacht as a marker on the markers layer
	 * adds a pop up box with the message to that marker
	 * if the position is estimated (CEP) then use the yacht with a red ring around it
	 * TODO: make ring the size of the CEP
	 */
	function addMarker(layer, lon, lat, popupContentHTML, distanceRun, cep) {
			
		if(cep == null) {
			var size = new OpenLayers.Size(21,25);
			var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
			var icon = new OpenLayers.Icon('yacht_icon.png', size, offset);
		} else { 
			var size = new OpenLayers.Size(30,30);
			var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
			var icon = new OpenLayers.Icon('yacht_cep.png', size, offset);
		}


		var ll = new OpenLayers.LonLat(Lon2Merc(lon), Lat2Merc(lat));
		marker = new OpenLayers.Marker(ll, icon.clone());

		
		var feature = new OpenLayers.Feature(layer, ll);
		feature.closeBox = true;
		feature.popupClass = OpenLayers.Class(OpenLayers.Popup.FramedCloud, {minSize: new OpenLayers.Size(260, 100) } );
		feature.data.popupContentHTML = popupContentHTML + " Total: " + distanceRun + "NM";
		feature.data.overflow = "hidden";

		marker.feature = feature;

		var markerClick = function(evt) {
			if (this.popup == null) {
				this.popup = this.createPopup(this.closeBox);
				map.addPopup(this.popup);
				this.popup.show();
			} else {
				this.popup.toggle();
			}
			OpenLayers.Event.stop(evt);
		};
		marker.events.register("mousedown", feature, markerClick);
		layer.addMarker(marker);
	}
	/**
	 * Pull in sea marks from the Open Sea Map site
	 */
	function getTileURL(bounds) {
		var res = this.map.getResolution();
		var x = Math.round((bounds.left - this.maxExtent.left) / (res * this.tileSize.w));
		var y = Math.round((this.maxExtent.top - bounds.top) / (res * this.tileSize.h));
		var z = this.map.getZoom();
		var limit = Math.pow(2, z);
		if (y < 0 || y >= limit) {
			return null;
		} else {
			x = ((x % limit) + limit) % limit;
			url = this.url;
			path= z + "/" + x + "/" + y + "." + this.type;
			if (url instanceof Array) {
				url = this.selectUrl(path, url);
			}
			return url+path;
		}
	}
	function drawmap() {

		map = new OpenLayers.Map('map', {
			projection: new OpenLayers.Projection("EPSG:900913"),
			displayProjection: new OpenLayers.Projection("EPSG:4326"),
			controls: [
				new OpenLayers.Control.Navigation(),
				new OpenLayers.Control.ScaleLine({topOutUnits : "nmi", bottomOutUnits: "km", topInUnits: 'nmi', bottomInUnits: 'km', maxWidth: '40'}),
				new OpenLayers.Control.LayerSwitcher(),
				new OpenLayers.Control.MousePosition(),
				new OpenLayers.Control.PanZoomBar()
			],
			maxExtent:new OpenLayers.Bounds(-20037508.34, -20037508.34, 20037508.34, 20037508.34), 
			numZoomLevels: 18,
			maxResolution: 156543,
			units: 'meters'
		});

		// Add Layers to map. Harbours and marks aren't strictly necessary
		// Mapnik
		layer_mapnik = new OpenLayers.Layer.OSM.Mapnik("Mapnik");
		// Seamark
		layer_seamark = new OpenLayers.Layer.TMS("Sea Marks", "http://tiles.openseamap.org/seamark/", 
			{
				numZoomLevels: 18, type: 'png', getURL: getTileURL, isBaseLayer: false, displayOutsideMaxExtent: true
			});
		// Harbours
		layer_pois = new OpenLayers.Layer.Vector("Harbour", 
			{
				projection: new OpenLayers.Projection("EPSG:4326"), visibility: true, displayOutsideMaxExtent:true
			});
		layer_pois.setOpacity(0.8);
		
		//marker_layer
		layer_markers = new OpenLayers.Layer.Markers( "Markers" );
		var points = [
		{"p":[37924792,-122374917],"m":"Mast is back on and we are going to leave < 24 hours!", }
		//TODO if the JS was correct then this would be a simple include!
		<?php 
		include 'positions.js';
		?>
		];
		points.forEach(function(point, index, array) {
			if(index != 0) {
				totalDistance += distanceRun((points[index -1]['p'][1]/1000000), (points[index -1]['p'][0]/1000000), 
						(point['p'][1]/1000000), (point['p'][0]/1000000));
			}
			addMarker(layer_markers,  (point['p'][1]/1000000), (point['p'][0]/1000000), point['transmit_time'] + ": " + point['m'], totalDistance, point['iridium_cep']);
			console.log(point['p'][1], point['p'][0], point['m']);
			lon = point['p'][1]/1000000;
			lat = point['p'][0]/1000000;
		});
		map.addLayers([layer_mapnik, layer_seamark, layer_pois, layer_markers]);
		jumpTo(lon, lat, zoom);
	}
  </script>
</head>
<!-- body.onload draws our map -->
<body onload="drawmap();">
  <h2>Track of the Sailing Yacht Flutterby</h2>
  <!-- define a DIV into which the map will appear. Make it take up the whole window -->
  <div style="width:100%; height:100%" id="map"></div>
</body>
</html>
