<?php
/**
 * Takes a PUT request from Iridium tracker and writes it to file
 * warning, no taint checks are done, so both file and JSON string 
 * might be corrupt!
 * BCK 2018-12-03
 */
$file = 'positions.js';
$imei = $_POST["imei"];
$momsn = $_POST["momsn"];
$transmit_time = $_POST["transmit_time"];
$iridium_latitude = $_POST["iridium_latitude"];
$iridium_longitude = $_POST["iridium_longitude"];
$iridium_cep = $_POST["iridium_cep"];
$data = $_POST["data"];
 
$decoded = pack('H*', $data);
//echo($decoded);
$message = json_decode($decoded);
if($message === null) {
  //TODO: we need to check decoded is a valid JSON string, as it might contain " within the "m" = "" part!
  //TODO: deal with JSON errors, regexp and format "m" correctly.
  $message->{'m'} = "Could not decode message.";
}
//If the GPS failed, we use the approximate position from Iridium, and report the CEP
if ($message->{'p'}[0] == 0 or $message->{'p'}[1] == 0) {
	$message->{'p'}[0] = floatval($iridium_latitude)*1000000; //we're working in microdegrees
	$message->{'p'}[1] = floatval($iridium_longitude)*1000000;
	$message->{'iridium_cep'} = floatval($iridium_cep); //extra field, telling JS approx location is within the "centre of estimated position"
} 
$message->{'transmit_time'} = $transmit_time;
// Write the contents back to the file. The comma is a crude way to append to an array - See JS to see how this is handled
// FIXME: read in previous contents, and append to a JSON array properly
if(file_put_contents($file, "," . json_encode($message) . "\n" , FILE_APPEND | LOCK_EX)) {
	echo "OK"; //needed by Iridium service
}
//TODO: better error reporting
?>
